= 安めのスマートウォッチをOSSでセキュアに使う(Gadgetbridge)
Kenichiro Matohara(matoken) <matoken@kagolug.org>
:revnumber: 1.0
:revdate: 2023-09-17(sun)
:revremark: 「{doctitle}」
:homepage: https://matoken.org/
:imagesdir: resources
:data-uri:
:example-caption: 例
:table-caption: 表
:figure-caption: 図
:backend: revealjs
:revealjs_theme: serif
:customcss: resources/my-css.css
:revealjs_slideNumber: c/t
:title-slide-transition: none
:icons: font
:revealjsdir: reveal.js/
:revealjs_hash: true
:revealjs_center: true
:revealjs_autoPlayMedia: true
:revealjs_transition: false
:revealjs_transitionSpeed: fast
:revealjs_plugin_search: enabled

== link:https://matoken.org[Kenichiro Matohara(matoken) https://matoken.org]

image::map.jpg[background, size=cover]

* 南隅から参加(鹿児島の右下)
* 好きなLinuxディストリビューションはDebian
* お仕事募集 mailto:work＠matohara.org

== 先月の補足

「有線イヤホンのボタンで動画を操作したい」::
https://gitlab.com/matoken/kagolug-2023.08/-/blob/main/slide/slide.adoc  +
https://speakerdeck.com/matoken/i-want-to-operate-the-video-with-the-buttons-on-the-wired-earphones
* 3.5mm オーディオジャックに接続したマイク，ボタン付きイヤホンのボタンで動画の制御
** ウェブブラウザの動画再開時に画面がスリープのまま(mpvは画面復帰する)  +
`xset dpms force on` を追加して解決．

== スマートウォッチ購入

.2022年11月末に Xiaomi Mi スマートバンド5 日本語版 2694円で購入
----
【11/22-11/27日40%OFF】Xiaomi Mi ｽﾏｰﾄﾊﾞﾝﾄﾞ5 日本語版  ｽﾏｰﾄｳｫｯﾁ本体 50M防水 14日間の長いﾊﾞｯﾃﾘ持続時間 11種類のｽﾎﾟｰﾂﾓｰﾄﾞ 24時間心拍数ﾓﾆﾀﾘﾝｸﾞ LINE･ﾒｯｾｰｼﾞ･座りすぎ･着信通知 女性健康追跡  ﾀﾞｲﾅﾐｯｸｶﾗｰﾃﾞｨｽﾌﾟﾚｲ(28355)
https://item.rakuten.co.jp/xiaomiofficial/m00028/
2694(円)×1(個) = 2694(円) (税込､送料無料)
----

NOTE: 公式ストアでは在庫切れのままなので収束品?

== ファースト・インプレッション

* 小さい，厚い
* 液晶にドット抜けあり><
* 文字は案外読める
* 久々に腕時計をしたので結構邪魔
* 狭いところに手を入れるときなどにあたって邪魔
* 引っ掛けてバンドが外れてしまうことがよくある -> サードパーティバンド?
* 金属製の什器などにゴツゴツぶつけてしまっているが液晶はきれいなままで結構丈夫そう

=== !

image:miband5.jpg[]
image:miband5-thick.jpg[]
image:miband5-back.jpg[]

== スマート?ウォッチ

* 安めのものはスマートフォン&アプリが必須
** 無いとあまりスマートでないウォッチ
* スマートフォンのアプリは権限が大量に必要
** 通知，ヘルスデータなど結構センシティブな内容が見える
** Googleにはすでに渡っているだろうけど更に他の会社にデータを渡したくない

=== !

image:diagram.jpg[]

== Mi Band hack!

* Mi Band 3 をhack している人が  +
Linux からBluetooth 経由でメッセージを送ったり時計を設定したり，FW Update したりしている :)
** link:https://medium.com/@yogeshojha/i-hacked-xiaomi-miband-3-and-here-is-how-i-did-it-43d68c272391[BLE Hack, Yogesh Ojha, MiBand Hack, BLE MiBand Hack, IoT Security, BLE security | Medium]
** link:https://medium.com/@yogeshojha/i-hacked-miband-3-and-here-is-how-i-did-it-part-ii-reverse-engineering-to-upload-firmware-and-b28a05dfc308[I hacked MiBand 3, and here is how I did it Part II — Reverse Engineering to upload Firmware and Resources Over the Air | by Yogesh Ojha | Medium]

公式アプリを使わなくてもある程度使えそう -> 購入へ💸

== link:https://www.gadgetbridge.org/[Gadgetbridge for android]

* Android 向けのアカウントを作成したりデータの送信などをしないで一部(Pebble, Mi Band, Amazfit Bip and HPlus device (and more))スマートウォッチを利用できるようになるOSSアプリ
* すべての機能が使えるとは限らない

----
Supported Devices

  :

Mi

    Band, Band 1A, Band 1S, Band 2, Band 3
    Band 4, Band 5, Band 6 [!]
----

== Android へのGadgetbridge 導入

F-Droid から入手可能

* link:https://f-droid.org/packages/nodomain.freeyourgadget.gadgetbridge/[Gadgetbridge | F-Droid - Free and Open Source Android App Repository]

[NOTE]
--
F-Droid はFLOSS をホストするAndroid用リポジトリ，ストア
F-Droid( https://f-droid.org/ja/ )からAPK を入手，導入して利用する
--

=== サイドローディング

* link:https://business.nikkei.com/atcl/gen/19/00297/090700094/[政府検討のスマホアプリ「サイドローディング」義務化、セキュリティーは大丈夫か：日経ビジネス電子版]
* link:https://news.mynavi.jp/article/20220903-2443266/[スマホアプリの「サイドローディング」で得られるもの、失うもの | マイナビニュース]
* link:https://mobilelaby.com/blog-entry-android-14-could-prevent-sideloading-outdated-apps.html[Android 14で古いアプリのサイドローティングも制限か]

== 接続方法

* Gadgetbridge wiki に機種別の手順がある
** link:https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Mi-Band-5[Mi Band 5 - Gadgetbridge - Codeberg.org]

=== ペアリング

* (あれば)Android のペアリング設定から削除
* Gadgetbridge 前設定
** `ガジェットブリッジ -> 設定 -> Mi Band/Amazfitの設定 -> Bluetoothペアリングを有効にする`
* ペアリング
** `ガジェットブリッジ -> 新しいデバイスに接続`

=== 認証鍵の入手

* mi Band 4以降はデバイスの初期設定時にサーバーベース認証が必要🤔
* 公式アプリで認証後認証鍵を入手 ※失敗  +
`sqlite3 /data/data/com.xiaomi.hm.health/databases/origin_db_[YOURDBNAMEHERE] "select AUTHKEY from DEVICE"` 
* huami-token利用 ※失敗
** link:https://codeberg.org/argrento/huami-token[argrento/huami-token: Script to obtain watch or band bluetooth token from Huami servers - huami-token - Codeberg.org]
* Free my band( 純正アプリ改造品)利用 ※成功
** link:https://www.freemyband.com/[Free my band]

=== 認証鍵のGadgetbridgeへの登録

* `ガジェットブリッジ -> Mi Smart Band5 -> ⚙(Debice specific settings) -> Auth Key` に 入手した**認証鍵の頭に `0x` を付けて保存する**  +
ex) `fedcba01234567890fedcba01234567` -> `0xfedcba01234567890fedcba012345678`

NOTE: 0x に気づかず何度も試した……．

* 戻ってデバイス名タップでペアリングされる
* ↻アイコンタップで手動データ同期

== Gadgetbridge で時計のデータを表示できた :)

image:gadgetbrigde-sync.jpg[width=70%]

== 初期化時の問題⚠

* スマートウォッチを初期化するとBluetooth MAC address が変わってしまう
* ペアリングのやり直しが必要
* 認証鍵も取得し直す必要がある

== いくつかの設定

=== デバイスの設定

* Heart Rate Monitoring
** 終日心拍数測定: 5分ごと(1分に1回，5分ごと，10分ごと，30分ごと)
* 持ち上げ時に表示を有効にする
* カレンダーの予定を同期する(不安定?)
* サードパーティのリアルタイムHRアクセス

=== ガジェットブリッジ設定

* データベース管理 -> 自動エクスポート
* 一般ステータス通知対応 -> ガジェットブリッジを許可
* Application list -> 通知を転送したいアプリを選ぶ

:

== こんな感じで利用中

image:mi-notification.jpg[]
image:mi-totp.jpg[]

== 数ヶ月使ってみて

=== バッテリーの持ち

* カタログスペック -> バッテリ持続時間: 14 日以上
* 現在の利用方法では実測10日以上
** 液晶輝度 2 of 5，心拍数測定5分ごと，持ち上げ時に表示を有効にする，通知は10通以下/日，HR自動エクスポート

NOTE: 充電には専用USBケーブルで充電が必要

=== 防水について

* 5ATM(50m防水)
** シャワーや水泳も可能となっている
* シャワー程度ならつけっぱなしで湯船に浸かるときと充電時以外ずっとつけっぱなしだが今のところ問題なし

=== ヘルスデータの精度

* HRは正確そうな値
* 歩数カウントは全然カウントされない?Androidの数分の一? -> バンドをぴったりめにすることでマシに
* 睡眠は全く当てにならない感じ

NOTE: HRのデータはAndroidに自動保存可能

== まとめ

* OSS のAndroid アプリのGadgetbridge を利用して一部のスマートウォッチが利用可能
* アカウントを作成したりデータを渡す心配なく利用できる
* 全機能が使えない場合も
* 壊れた場合時計と違い買い替えになると思うが同じ価格帯で代替品が買えるかが不安
* link:https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/New-Device-Tutorial[Gadgetbridge非対応ウォッチを持っていたら対応できるか試すのも面白そう]

== SEE ALSO

安価でOpen Hardware な物もある(ESP32ベースのものは技適もありそう?)

* link:https://www.pine64.org/pinetime/[PineTime | PINE64]
* link:https://www.lilygo.cc/products/t-watch-s3?variant=42884364009653[T-Watch S3 – LILYGO®]
* link:https://www.crowdsupply.com/sqfmi/watchy[Watchy | Crowd Supply]

=== 発表後追記

* 前ページで紹介したものは全て技適的にNGだった
** ESP32でもアンテナ別の小型chipなので技適なし
* 以下のあたりなら丈夫そうだが大きい
** link:https://hackaday.io/project/168227-custom-smartwatch[Custom Smartwatch | Hackaday.io]
** link:https://www.switch-science.com/products/7535[M5StickC Plus（腕時計アクセサリー付き） — スイッチサイエンス]

== 奥付

発表::
* link:https://kagolug.connpass.com/event/295032/[鹿児島Linux勉強会 2023.09(オンライン開催)] 2023-09-17(sun)
発表者::
* link:https://matoken.org/[Kenichiro Matohara(matoken)]
利用ソフトウェア::
* link:https://github.com/asciidoctor/asciidoctor-reveal.js[Asciidoctor Reveal.js]
ライセンス::
* link:https://creativecommons.org/licenses/by/4.0/[CC BY 4.0]

