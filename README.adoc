= 鹿児島Linux勉強会 2023.09(オンライン開催)

日時:: 2023-09-17(sun) 14:00-18:00 ( 〜 21:10 )
会場:: Self Hosted Galene
URL:: https://kagolug.connpass.com/event/295032/
参加者:: 4人

== matoken発表

* 「安めのスマートウォッチをOSSでセキュアに使う(Gadgetbridge)」
** https://gitlab.com/matoken/kagolug-2023-09/-/blob/main/slide/slide.adoc
** https://speakerdeck.com/matoken/securely-use-cheap-smart-watches-with-oss-gadgetbridge
* 「YoutubeをWebpageに変換するYoutube2Webpage」
** https://matoken.org/blog/2023/09/07/youtube2webpage/

== 通信量

----
$ vnstat -h -b '2023-09-17 14:00' -e '2023-09-17 21:10'

 wlp2s0  /  hourly

         hour        rx      |     tx      |    total    |   avg. rate
     ------------------------+-------------+-------------+---------------
     2023-09-17
         14:00    123.46 MiB |   66.39 MiB |  189.85 MiB |  442.39 kbit/s
         15:00    209.24 MiB |  144.95 MiB |  354.18 MiB |  825.30 kbit/s
         16:00    237.02 MiB |  132.85 MiB |  369.87 MiB |  861.86 kbit/s
         17:00    249.00 MiB |  101.62 MiB |  350.62 MiB |  817.01 kbit/s
         18:00    242.08 MiB |   82.61 MiB |  324.69 MiB |  756.59 kbit/s
         19:00    334.16 MiB |  108.09 MiB |  442.25 MiB |    1.03 Mbit/s
         20:00    407.92 MiB |  101.65 MiB |  509.56 MiB |    1.19 Mbit/s
         21:00     25.70 MiB |   35.15 MiB |   60.84 MiB |  141.78 kbit/s
     ------------------------+-------------+-------------+---------------
      sum of 8      1.79 GiB |  773.30 MiB |    2.54 GiB |

----

